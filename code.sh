#! /bin/bash


# installation
curl -L https://tljh.jupyter.org/bootstrap.py   | sudo python3 -   --admin admin1


sudo tljh-config set auth.type firstuseauthenticator.FirstUseAuthenticator
sudo tljh-config set auth.FirstUseAuthenticator.create_users true
sudo tljh-config reload
# affiche les logs
13  sudo tljh-config show

# logs de l'installation
sudo cat /opt/tljh/installer.log


sudo journalctl --follow  -u jupyterhub


# Installer quelques librairies

sudo /opt/tljh/user/bin/python -m pip install geopandas
## semble rouler dans un environnement virtuel
