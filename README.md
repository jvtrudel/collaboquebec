# Collaborer sur les données ouvertes du Québec

Prototype de plateforme collaborative sur les données ouvertes du Québec

## Portée de la démonsration

  - [x] Installer une instance publique de JupyterHub
  - [x] Multi-utilisateurs
  - [] Fournit les librairies de base de manipulation et de visualisation de données (python, Julia, R)
  - [] Fournir des données ouvertes pré-téléchargés
  - [] Les utilisateurs peuvent partager leurs notebooks, leurs données et d'autres assets
  - [] Des utilisateurs peuvent travailler sur les mêmes données



# Présentation

## Pourquoi collaborer sur les données ouvertes

  - on a tous un ou plusieurs projet impliquant des données ouvertes qui traine sur notre ordinateur
  - ensemble on va plus loin
  - montrer aux municipalités et aux ministères que leurs jeux de données sont "cool"
  - Carrément une interface de collaboration directe avec les municipalités et les ministères
  - Pour Drave Développement, un candidat idéal pour un projet de services infonuagique en gestion mutualisé

## Pourquoi Jupyter?

  - Kaggle (plateforme de compétition sur data science et AI) roule Jupyter... ils viennent de se faire acheter par Google!
  - Roule python
  - Utilisé dans le milieu académique (recherche et enseignement/formaiton)

##
